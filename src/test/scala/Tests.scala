import org.scalatest._;
import math.sqrt;

class Tests extends UnitSpec {

  "Distance of an empty segment" should "equal 0" in {
    var s = new Segment();
    assert(s.distance == 0);
  }
  "Distance between a(-4,4) and b(-1,4)" should "equal 3" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var s = new Segment(a,b);
    assert(s.distance == 3);
  }

  "Distance between c(0,0) and d(1,1)" should "equal sqrt(2)" in {
    var c = new Point(0,0);
    var d = new Point(1,1);
    var s2 = new Segment(c,d);
    assert(s2.distance == sqrt(2));
  }

  "Distance of an empty path" should "equal 0" in {
    var p = new Path(List());
    assert(p.distance() == 0);
  }

  "Length of Path: a->b" should "equal 3" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var s = new Segment(a,b);
    var p = new Path(List(s));

    assert(p.distance() == 3);
  }

  "Length of Path: a->b + b->c + c->d" should "equal 3 + sqrt(17) + sqrt(2) #= 8.537" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p = new Path(List(s,s2,s3));

    assert(p.distance() == (3 + sqrt(17) + sqrt(2)));
  }

  "Shortest path in empty list" should "be None" in {
    var pl = new PathList();

    assert(pl.shortestPath() == None);
  }

  "Distance of shortest path in list with empty paths" should "equal 0" in {
    var p = new Path(List());
    var p1 = new Path(List());
    var p2 = new Path(List());
    var p3 = new Path(List());
    var p4 = new Path(List());
    var pl = new PathList(List(p,p1,p2,p3,p4));

    assert(pl.shortestPath().get.distance() == 0);
  }

  "Shortest path of list with only one path" should "be this path" in {
    var p = new Path(List());
    var pl = new PathList(List(p));

    assert(pl.shortestPath() == Some(p));
  }

  "Shortest path in (a->b + b->c + c->d, d->a), (c->d), (a->c)" should "be c->d with length = sqrt(2)" in {

    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p2 = new Path(List(s,s2,s3,new Segment(d,a)));
    var p3 = new Path(List(s3));
    var p4 = new Path(List(new Segment(a,c)));

    var pl = new PathList(List(p2,p3,p4));

    assert(pl.shortestPath().get.distance() == sqrt(2));
  }

  "getStops() in empty path" should "return an empty list" in {
    var p = new Path(List());

    assert(p.getStops() == List());
  }

  "enumerateStops() in (a->b,b->c,c->d)" should "return b and c" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p = new Path(List(s,s2,s3));

    var stopsList = p.getStops;
    assert(stopsList.contains(b) && stopsList.contains(c));
  }

  "pathWithStops()" should "give a path with all stops given" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p = new Path(List(s,s2,s3));
    var p2 = new Path(List(s3));
    var p3 = new Path(List(s2));

    var pl = new PathList(List(p,p2,p3));

    var plWithStops = pl.pathsWithStops(List(b,c));
    assert(plWithStops.contains(p) && ! plWithStops.contains(p2) && ! plWithStops.contains(p3));
  }

  it should "give an empty PathList if list is empty" in {
    var a = new Point(-4,4);

    var pl = new PathList();

    assert(pl.pathsWithStops(List(a)).paths == List());
  }

  "shortestPath in empty list of paths" should "return None" in {
    var pl = new PathList();

    assert(pl.shortestPath() == None);
  }

  "shortestPath in list with not all stops" should "return None" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p = new Path(List(s));
    var p2 = new Path(List(s3));
    var p3 = new Path(List(s2));

    var pl = new PathList(List(p,p2,p3));

    var plWithStops = pl.pathsWithStops(List(b,c));

    assert(plWithStops.shortestPath() == None);
  }

  "shortestPath in list with a single path which include all stops" should "return this path" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);

    var p = new Path(List(s,s2,s3));
    var p2 = new Path(List(s3));
    var p3 = new Path(List(s2));

    var pl = new PathList(List(p));

    var plWithStops = pl.pathsWithStops(List(b,c));

    assert(p.equals(plWithStops.shortestPath()));
  }

  "shortestPath in list with multiple paths with stops" should "return the shortest with the stops" in {
    var a = new Point(-4,4);
    var b = new Point(-1,4);
    var c = new Point(0,0);
    var d = new Point(1,1);
    var e = new Point(15,18);

    var s = new Segment(a,b);
    var s2 = new Segment(b,c);
    var s3 = new Segment(c,d);
    var s4 = new Segment(d,e);

    var p = new Path(List(s,s2,s3));
    var p2 = new Path(List(s3));
    var p3 = new Path(List(s,s2,s3,s4));

    var pl = new PathList(List(p,p2,p3));

    var plWithStops = pl.pathsWithStops(List(b,c));

    assert(p.equals(plWithStops.shortestPath()));
  }
}
