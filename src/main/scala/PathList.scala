class PathList(var paths: List[Path] = List()) {
  def equals(pl: PathList): Boolean = pl.paths.sameElements(this.paths);
  def shortestPath(i: Int = 0, currentShortest: Option[Path] = None, l: List[Path] = this.paths): Option[Path] = {
    if(paths.isEmpty) None;
    else {
      if(l.isDefinedAt(i+1)) {
        if(currentShortest != None) {
          if(currentShortest.get.distance() < l.apply(i).distance()) shortestPath(i+1,currentShortest);
          else shortestPath(i+1,Option(l.apply(i)));
        }
        else {
          shortestPath(i+1,Option(l.apply(i)));
        }
      }
      else if (l.isDefinedAt(i)) {
        if(currentShortest != None) {
          if(currentShortest.get.distance() < l.apply(i).distance()) currentShortest;
          else Option(l.apply(i));
        }
        else Option(l.apply(i));
      }
      else {
        None;
      }
    }
  };

  def pathsWithStops(s: List[Point], listOK: List[Path] = List(), i: Int = 0): PathList = {
    if(paths.isEmpty) this;
    else {
      if(paths.isDefinedAt(i+1)) {
        if(paths.apply(i).hasManyStops(s)) this.pathsWithStops(s, listOK :+ paths.apply(i), i+1);
        else this.pathsWithStops(s, listOK, i+1);
      }
      else {
        if(paths.apply(i).hasManyStops(s)) new PathList(listOK :+ paths.apply(i));
        else new PathList(listOK);
      }
    }
  };

  def contains(p: Path) = paths.contains(p);

  override def toString(): String = this.getString();

  def getString(i: Int = 0): String = {
    if(paths.isDefinedAt(i+1)) {
      if(i == 0) "/**\n" + this.paths.apply(i).toString + this.getString(i+1);
      else this.getString(i+1);
    }
    else {
      if(i == 0) "/**\n" + this.paths.apply(i).toString + "\n**/";
      else this.paths.apply(i).toString + "\n**/";
    }
  }
}
