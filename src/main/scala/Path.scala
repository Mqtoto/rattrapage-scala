class Path(var segments: List[Segment] = List(), var stops: List[Point]) {
  def this(segments: List[Segment]) = {
    this(segments, List());
    this.stops = this.findStops();
  }

  override def toString(): String = this.getString();

  def getString(i: Int = 0): String = {
    if(segments.isDefinedAt(i+1)) {
      if(i == 0) "[\n" + this.segments.apply(i).toString + this.getString(i+1);
      else this.getString(i+1);
    }
    else {
      if(i == 0) "[\n" + this.segments.apply(i).toString + "\n]";
      else this.segments.apply(i).toString + "\n]";
    }
  }

  def distance(i: Int = 0, result: Double = 0.0): Double = {
    if(segments.isEmpty) 0;
    else {
      if(segments.isDefinedAt(i+1)) distance(i+1,result+segments.apply(i).distance);
      else result+segments.apply(i).distance;
    }
  };

  def equals(p: Path): Boolean = {
    if(this.segments.sameElements(p.segments)) true;
    else false;
  }

  def equals(o: Option[Path]): Boolean = {
    if(o != None) {
      if(this.segments.sameElements(o.get.segments)) true;
      else false;
    }
    else {
      if(this.segments.isEmpty) true;
      else false;
    }
  }

  def findStops(i: Int = 0, currentList: List[Point] = List(), firstSegment: Boolean = true): List[Point] = {
    if(this.segments.isDefinedAt(i+1)) {
      if(firstSegment) {
        findStops(i+1, currentList :+ this.segments.apply(i).destination, false);
      }
      else {
        findStops(i+1, currentList :+ this.segments.apply(i).origin :+ this.segments.apply(i).destination, false);
      }
    }
    else {
      if(firstSegment) {
        List();
      }
      else {
        currentList :+ this.segments.apply(i).origin;
      }
    }
  };

  def getStops(): List[Point] = this.stops;

  def enumerateStops(): String = this.stops.toString();

  def hasStop(p: Point): Boolean = this.stops.contains(p);

  def hasManyStops(l: List[Point], i: Int = 0, has: Boolean = true): Boolean = {
    if(l.isDefinedAt(i+1)) {
      if(has) {
        if(this.hasStop(l.apply(i))) this.hasManyStops(l, i+1, true);
        else false;
      }
      else false;
    }
    else this.hasStop(l.apply(i));
  };
}
