class Point(var x: Int, var y: Int) {
  override def toString(): String = "(" + x + "," + y + ")";
  def equals(p: Point): Boolean = {
    if(p.x == this.x && p.y == this.y) true;
    else false;
  }
}
