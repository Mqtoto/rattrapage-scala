import math.sqrt;

class Segment(var origin: Point, var destination: Point) {
  def this() = this(new Point(0,0), new Point(0,0));
  def square(i: Double): Double = i * i;

  def distance: Double = math.sqrt(square(this.origin.x - this.destination.x) + square(this.origin.y - this.destination.y));

  def equals(s: Segment): Boolean = {
    if(this.origin.equals(s.origin) && this.destination.equals(s.destination)) true;
    else false;
  }

  override def toString(): String = "{" + this.origin + "," + this.destination + "}";
}
